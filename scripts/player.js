var ytube = angular.module("ytube",[]);


ytube.service('youtubeapi',['$window','$q', function($window, $q){

    var iframeAPIDeferred = null;
    
    this.youtubeIframeAPILoaded = function(){

        
        if(!iframeAPIDeferred){
            iframeAPIDeferred = $q.defer();
            console.log("API Loading");
            
            var tag = document.createElement('script');
            tag.src = 'http://www.youtube.com/iframe_api';
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            
            $window.onYouTubeIframeAPIReady = function(){
                console.log("API Loaded");
                iframeAPIDeferred.resolve();       
            }
            
        }
        return iframeAPIDeferred.promise;
    }
}]);

ytube.directive('youtube', function($window,youtubeapi,$compile) {
  return {
    restrict: "E",
    
    scope : {
        videoid : '@videoid'
    },
    
    controller : 'youtubeCtrl',
   
    template: '<div id="player"></div>',

    link: function(scope, element,attr,controller) {
      
        var player;
    
        youtubeapi.youtubeIframeAPILoaded().then(function(){
            
            
            console.log(scope.videoid);
                player = new YT.Player('player', {
                    playerVars: {
                    autoplay: 0,
                    html5: 1,
                    modesbranding: 0,
                    iv_load_policy: 3,
                    showinfo: 0,
                    controls: 0,
                    disablekb : 1
                },

                height: '720',
                width: '1280',
                videoId: scope.videoid,
                events : {
                    'onReady' : onPlayerReady,
                    'onPlaybackQualityChange' : onQualityChange,
                    'onStateChange' : onStateChange,
                }
            });
            
            controller.log("Player Loaded");
            controller.setPlayer(player);
            
            
            
        },
        function(){
            alert("Ni ho paya");
        });
        
        onPlayerReady = function(){
            
            player.setPlaybackQuality('medium');
            var controlEle = angular.element("<controls>Controls</controls>");
            element.append(controlEle);
            $compile(controlEle)(scope);
            controller.playerReady(true);
            //controller.playVideo();
        }
        
        onQualityChange = function(){
            controller.onQualityChange();   
        }
        
        onStateChange = function(){
            controller.onStateChange();   
        }
    } 
  }
});

ytube.controller('youtubeCtrl',function($scope, $timeout){
    var self = this;
    var _player = null;
    var isPlayerReady = false;
    
    this.log = function(msg){
        console.log("youtubeCtrl Log : " + msg);   
    }

    this.playVideo = function(){
        if(_player && isPlayerReady){
            _player.playVideo();    
        }
        
       
    }
    
    this.pauseVideo = function(){
        if(_player && isPlayerReady){
            _player.pauseVideo();  
        } 
        
    }
    
    this.toggleVideo = function(){
        if(this.getPlayerState() == 1 ){
            this.pauseVideo();
            return 2;
        }
        else{
            this.playVideo();
            return 1;
        }
        
        
        
    }

    this.setPlayer = function(player){
        _player = player;
    }

    this.playerReady = function(ready){
        isPlayerReady = ready;   
        this.log("PlayerReady : " + ready);
        $timeout(function(){
            $scope.$broadcast('playerReady', true);
        },100);
        
    }
    
    this.logPlayerState = function(){
        this.log("PlayerState : " + isPlayerReady);
        
    }
    
    this.getPlayerState = function(){
        return _player.getPlayerState();   
    }
    
    this.toggleQuality = function(){
        var quality = _player.getPlaybackQuality();
        if(quality == 'medium'){
            _player.setPlaybackQuality('hd720');   
        }else
            _player.setPlaybackQuality('medium');
        this.log("QualityToggled");
    }
    
    this.onQualityChange = function(){
        var quality = _player.getPlaybackQuality();
        $scope.$broadcast('qualityChange', quality); 
        this.log("QualityChange " + quality);
    }
    
    
    
    this.getDuration = function(){
        return _player.getDuration();  
    }
    
    this.getCurrentTime = function(){
        return _player.getCurrentTime();   
    }
    
    this.seekTo = function(seconds, allowSeekAhead){
        _player.seekTo(seconds,allowSeekAhead);
    };
    
    this.onStateChange = function(){
        $scope.$broadcast('stateChange', _player.getPlayerState());
    }
    
    this.setVolume = function(value){
        _player.setVolume(value);   
    }
    
    this.getVolume = function(){
        return _player.getVolume();
    }

   
});

ytube.directive('controls', function($timeout,$interval,$compile){
    
    return{
        require  : '^youtube',
        controller :function($scope) {
            $scope.duration = 272;
            
            $scope.getProperTime = function(seconds){
                var time = {};
                
                time.minutes = Math.floor(seconds/60);
                time.seconds = Math.floor(seconds % 60);
                return time;
            }
            
        },
        
        link : function(scope, element, attr, controller){
            scope.playing = false;
            scope.btnSrc="img/play-btn.png";
            scope.qualityBtnSrc="img/hd-btn.png";
            scope.currentTime = 0;
            scope.currentVolume = 50;
            scope.duration = 200;
            console.log("Link FUnction");
            scope.current = {
                minutes : 0,
                seconds : 0
            };
            scope.total = scope.getProperTime(200);
            
            
            scope.$on('playerReady', function(event,value){
                console.log("Player in ready ");
                scope.currentTime = controller.getCurrentTime();
                scope.current = scope.getProperTime(scope.currentTime);
                scope.duration = controller.getDuration();
                scope.total = scope.getProperTime(scope.duration);
                console.log("duration " + scope.duration);
                scope.currentVolume = controller.getVolume();
               
                console.log("adding via event");
                var sliderEle = angular.element('<div slider min="0" max="{{duration}}" model ="{{currentTime}}" onchange="changeCurrentTime(value)" class="slider"></div><div  slider id = "volume" min = "0" max = "100" model="{{currentVolume}}" onchange="changeCurrentVolume(value)"  ></div>');
                element.append(sliderEle);
                $compile(sliderEle)(scope);
            });
       
            
            $interval(updateTime,1000);
            
            function updateTime(){
                scope.currentTime = controller.getCurrentTime();
                scope.current = scope.getProperTime(scope.currentTime);
            }
            scope.toggleVideo = function(){
                
                if(controller.toggleVideo() == 2){
                    
                    scope.btnSrc="img/play-btn.png";
                    element.addClass("dark-bg");
                    scope.showOverflow = true;
                }
                else{
                    element.removeClass("dark-bg");
                    scope.btnSrc="img/pause-btn.png";
                    scope.showOverflow = false;
                }
            }
            
            scope.toggleQuality = function(){
                controller.toggleQuality();
            }
            
            scope.$on('stateChange', function(event, state){
                if(state == 3 || state == 1){
                    element.removeClass("dark-bg");
                    scope.btnSrc="img/pause-btn.png";
                    scope.showOverflow = false;
                } 
                else{
                    
                    scope.btnSrc="img/play-btn.png";
                    element.addClass("dark-bg");
                    scope.showOverflow = true;
                }
            });
            
            scope.$on('qualityChange', function(event, quality){
                controller.log("Event Recieved " + quality );
                if(quality == 'medium'){
                    scope.$apply(function(){
                       scope.qualityBtnSrc = 'img/non-hd-btn.png'; 
                        controller.log("non hd background set");
                    });
                    
                    
                }
                if(quality == 'hd720'){
                    scope.$apply(function(){
                       scope.qualityBtnSrc = 'img/hd-btn.png'; 
                        controller.log("non hd background set");
                    });
                }
            });
            
            scope.changeCurrentTime = function(value){
                controller.seekTo(value,true);  
            }
            
            scope.changeCurrentVolume = function (value){
                controller.setVolume(value);   
            }
        },
        templateUrl : 'buttons.html',
        
    }
});




ytube.directive('slider',function($timeout){
    return {
        restrict: 'A',
        require : '^controls',
        scope: {
            min : "=min",
            max: "@max",
            model: "@model",
            onchange : "&onchange"
        },
        link: function(scope, elem, attrs) {
            
                
                
                scope.$watch(function(scope){
                    return scope.model;
                }, function(value){
                    setModel(value);
                });
           
          
                console.log("max " + scope.max);
               $(elem).slider({
                range: "min",
	            max: scope.max,
	            min: scope.min,
                step: 1,
                stop: function(event, ui) { 
                    scope.onchange({value : ui.value});
                    console.log("changing to " + ui.value);
	            }
                   
                  
	        });
             
            $('.ui-slider-handle').css('height','25px');
            $('.ui-slider-handle').css('width','25px');
            $('.ui-slider-handle').css('background','url("img/handle.png")');
            $('.ui-slider-handle').css('border','none');
            $('.ui-slider-handle').css('top','-10px');
            
            var setModel = function(value){
               
                $(elem).slider('value',value);
            }
            
            
            
    	}
    }
});


